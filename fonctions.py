import csv
import os
from datetime import datetime

listeFormatDestination = [
    'adresse_titulaire',
    'nom',
    'prenom', 
    'immatriculation' , 
    'date_immatriculation', 
    'vin', 
    'marque', 
    'denomination_commerciale', 
    'couleur', 
    'carrosserie',
    'categorie', 
    'cylindree', 
    'energie', 
    'places', 
    'poids', 
    'puissance',
    'type',
    'variante',
    'version'
]

#Cette fonction permet d'initialiser une liste de dictionnaires contenant les données des véhicules
#Elle prend en paramètre le chemin d'accès du fichier
#Elle renvoie la liste de dictionnaires
#A chaque itération sur une ligne du descripteur, on créé un dictionnaire que l'on vient 
def initListeDonneesToModify(cheminAccesSource):
    if os.path.exists(cheminAccesSource) and os.path.isfile(cheminAccesSource):
        listeDonneesInitiales = []
        with open(cheminAccesSource) as fichierCourant:
            contenuFichier = csv.reader(fichierCourant, delimiter = "|")
            for row in contenuFichier:
                dicoDonneesSource = {
                    'address':row[0], 
                    'carrosserie':row[1], 
                    'categorie':row[2], 
                    'couleur':row[3], 
                    'cylindree':row[4], 
                    'date_immat':row[5], 
                    'denomination':row[6], 
                    'energy':row[7], 
                    'firstname':row[8], 
                    'immat':row[9], 
                    'marque':row[10], 
                    'name':row[11], 
                    'places':row[12], 
                    'poids':row[13], 
                    'puissance':row[14], 
                    'type_variante_version':row[15], 
                    'vin':row[16]
                }
                listeDonneesInitiales.append(dicoDonneesSource)

    return listeDonneesInitiales


#Cette fonction permet d'initialiser le fichier destination avec les données au bon format, pour sa lecture par la seconde application
#Cette fonction prend en paramètre : le chemin d'accès au fichier source et le chemin d'accès au fichier de destination
#Une vérification doit être réalisée pour savoir si les deux fichiers existent et sont bien au bon format
#La fonction renvoie UNE LISTE DE DICTIONNAIRE CONFORME AU FORMAT DE DESTINATION
#A chaque itération sur une ligne de la liste de données sources, on va construire un nouveau dictionnaire contenant les infos au bon format.
#Pour la seconde TMA, on doit convertir un format de date in en un format out. On utilise alors les méthodes datetime, strptime et strptime
def initListeDonneesBonFormat(cheminAccesSource):
    
    listeDonneesFormatCorrect = []
    listeDonneesSources = initListeDonneesToModify(cheminAccesSource)
    for dictionnaire in listeDonneesSources[1:]: #Suppression de la première ligne qui contient le titre des champs
        colonneToSplit = list(str(dictionnaire['type_variante_version']).split(','))
        in_format = "%Y-%m-%d"
        out_format = "%d/%m/%Y"
 
        dictionnaireFormatDest = {
            'adresse_titulaire' : dictionnaire['address'],
            'nom' : dictionnaire['name'],
            'prenom' : dictionnaire['firstname'], 
            'immatriculation': dictionnaire['immat'] , 
            'date_immatriculation': datetime.strptime(dictionnaire['date_immat'], in_format).strftime(out_format), 
            'vin' : dictionnaire['vin'], 
            'marque': dictionnaire['marque'], 
            'denomination_commerciale': dictionnaire['denomination'], 
            'couleur': dictionnaire['couleur'], 
            'carrosserie': dictionnaire['carrosserie'],
            'categorie': dictionnaire['categorie'], 
            'cylindree': dictionnaire['cylindree'], 
            'energie': dictionnaire['energy'] , 
            'places': dictionnaire['places'], 
            'poids': dictionnaire['poids'], 
            'puissance': dictionnaire['puissance'],
            'type': colonneToSplit[0],
            'variante': colonneToSplit[1],
            'version': colonneToSplit[2]
        }
        listeDonneesFormatCorrect.append(dictionnaireFormatDest)

    return listeDonneesFormatCorrect

#La fonction permet la réécriture d'un fichier source au bon format dans un fichier destination
#La fonction prend en paramètre le chemin d'accès au fichier source et celui du fichier destination
#A l'issue de la première TMA, on ajoute un troisième séparateur qui correspond au caractère séparateur choisi par l'utilisateur
#La fonction ne retourne rien
def ecritureFichierDestination(cheminFichierSource, cheminFichierDest, caractereSeparateur):

    with open(cheminFichierDest, 'w', newline='') as fichierDestination:
        fieldnames = listeFormatDestination
        descripteurEcriture = csv.DictWriter(fichierDestination, fieldnames=fieldnames, delimiter=caractereSeparateur)
        
        descripteurEcriture.writeheader()

        for resultat in initListeDonneesBonFormat(cheminFichierSource):
            descripteurEcriture.writerow(resultat)

    print("La transformation du fichier a été réalisée avec succès !")